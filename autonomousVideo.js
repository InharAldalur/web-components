class AutonomousVideo extends HTMLElement
{
	constructor()
	{
		super();
		this.videos = [];
		this.index = 0;
	}

	async connectedCallback()
	{
		this.innerHTML += await fetch("autonomousVideo.html").then(r => r.text());
		this.video = this.querySelector("video");

		this.downloadVideos(video => this.videoDownloaded(video));

		this.video.addEventListener('ended', () => this.videoEnded());
	}

	async downloadVideos(downloadedCallback)
	{
		for (const source of this.querySelectorAll("source"))
		{
			const response = await fetch(source.getAttribute("src"));
			const blob = await response.blob();
			const src = URL.createObjectURL(blob);
			downloadedCallback(src);
		}
	}

	videoDownloaded(src)
	{
		this.videos.push(src);

		if (this.videos.length == 1)
		{
			this.video.src = src;
		}
	}

	videoEnded()
	{
		this.index = (this.index + 1) % this.videos.length;
		this.video.src = this.videos[this.index];
	}
}

customElements.define('autonomous-video', AutonomousVideo);
