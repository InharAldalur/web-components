# Web components

Web components allow creating encapsulated HTML elements that can be reused across projects.
There are two kinds of web components:

- **Autonomous** components define a brand new html tag that defines its own internal HTML structure.
- **Custom** components inherit from existing HTML elements and add functionality to them.

## Browser support

As of 2020, autonomous components are supported across all evergreen browsers.
Custom components are supported in Chromium and Firefox and Edge. Opera and Safari don't support them.

## Shadow vs Light DOM

## Styling: slots and parts

## Must defer JS execution until DOM is loaded
